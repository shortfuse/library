from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.contrib.auth import authenticate, login
from .forms import LoginForm

from .models import Account


def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(username=cd['username'], password=cd['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponse('Authenticated successfully')
                else:
                    return HttpResponse('Disabled account')
            else:
                return HttpResponse('Invalid login')
    else:
        form = LoginForm()
    return render(request, 'library/login.html', {'form': form})



def account_list(request):
    accounts = Account.objects.filter(status='Available')

    return render(request, 'library/account/list.html', {'accounts': accounts})


def account_detail(request, slug):
    account = get_object_or_404(Account, slug=slug)
    #cart_account_form = CartAddAccountForm()
    return render(request,
                  'library/account/detail.html',
                  {'account': account,  })
     #              'cart_account_form': cart_account_form})
