from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse


class Account(models.Model):
    ''' login accounts class '''
    account_name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, unique=True)
    account_pass = models.CharField(max_length=100)

    ACCOUNT_TYPE = (
        ('Safari', 'Safari'), ('Pluralsights', 'Pluralsights'),
    )

    type = models.CharField(
        max_length=100,
        choices=ACCOUNT_TYPE,
        blank=False,
        default='Safari',
        help_text='type',
    )

    account_taken_by = models.ForeignKey(User, models.SET_NULL, blank=True, null=True,)
    account_taken_at = models.DateTimeField('Account in use since', auto_now=True)

    ACCOUNT_STATUS = (
        ('Taken', 'Taken'),
        ('Available', 'Available'),
        ('Expired', 'Expired'),
    )

    status = models.CharField(
        max_length=50,
        choices=ACCOUNT_STATUS,
        blank=True,
        default='Taken',
        help_text='Account availability',
    )

    #class Meta:
    #    ordering = ["account_status", "-account_taken_at"]

    def __str__(self):
        ''' return account name '''
        return self.account_name
        
    def get_absolute_url(self):
        return reverse('account_detail', args=[self.slug])
