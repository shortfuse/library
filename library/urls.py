from django.urls import path, include
from . import views

urlpatterns = [
    path('accounts/', include('django.contrib.auth.urls')),
    #path('login/', views.user_login, name='login'),
    #path('login/', LoginView.as_view(template_name='library/login.html'), name='login'),
    path('', views.account_list, name='account_list'),
    path('<slug>/', views.account_detail, name='account_detail'),
]
